import React from 'react';
import Dashboard from './components/dashboard';
import './scss/app.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    return (
      <div className="app-section-main">
        <Dashboard />
      </div>
    )
  }
}

export default App;
