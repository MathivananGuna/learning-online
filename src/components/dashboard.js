import React from 'react';
import '../scss/dashboard.scss';
import '../scss/media-query.scss';
import commonTextFile from '../common/text';
import { Button } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
// import PersonAddIcon from '@material-ui/icons/PersonAdd';
// import PieChartIcon from '@material-ui/icons/PieChart';
// import MenuBookIcon from '@material-ui/icons/MenuBook';
// import logoImage from '../assets/logoPng.png';
// import introImg from '../assets/intro.png';
// import AboutImage from '../assets/about.png';
import SendIcon from '@material-ui/icons/Send';
// import AdbIcon from '@material-ui/icons/Adb';
// import BugReportIcon from '@material-ui/icons/BugReport';
// import BarChartIcon from '@material-ui/icons/BarChart';
// import LanguageIcon from '@material-ui/icons/Language';
// import MultilineChartIcon from '@material-ui/icons/MultilineChart';
import PhoneIcon from '@material-ui/icons/Phone';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import EmailIcon from '@material-ui/icons/Email';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
// import pythonImage from '../assets/python.jpg';
// import javaImage from '../assets/java.png';
// import dataAnalytics from '../assets/data-analytics.png';
// import testAutomation from '../assets/automation.jpg';
// import intelligenceAI from '../assets/Ai-1.png';
// import dataScience from '../assets/datascience.png';
// import fullStack from '../assets/fullstack.png';
import generalProgram from '../assets/generalProgram.png';
// import aptitudePrep from '../assets/aptitude.jpg';
// import businessAnalytics from '../assets/business.jpg';
import everyCourse from '../assets/everyCourse.png';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            everyCourse: ["Global Certification", "Life Time Access", "Doubt Clearing Sessions", "Module Based Exams", "Practice Tests", "Resume Preparation", "Mock Interviews", "100% Job Assistance", "Module Communities"],
            fieldWorks: ["BE", "B.Tech", "MCA", "BCA", "M.Tech", "MS"]
        }
    }
    connectToWhatsApp = (e) => {
        window.open("https://api.whatsapp.com/send?phone=+919741191117&text=Hi, Need more assistance from you. Thank You!")
    }
    connectToCall = () => {
        alert("Please feel free to contact us on +91-9741191116/7")
    }
    registerPopUp = () => {
        alert("For Registration Please contact us on +91-9741191116/7")
    }
    featureAlert = () => {
        alert("This Feature will be added soon")
    }
    faceBookRoute = () => {
        window.open("https://www.facebook.com/hirescripts.hs")
    }
    instPageRoute = () => {
        window.open("https://www.instagram.com/hire_scripts/")
    }
    render() {
        return (
            <div className='dashboard-section-main'>
                <div className="border-section">
                    <p className="top-left"></p>
                    <p className="top-right"></p>
                    <p className="bottom-left"></p>
                    <p className="bottom-right"></p>
                    <p className="right-top"></p>
                    <p className="left-top"></p>
                    <p className="left-bottom"></p>
                    <p className="right-bottom"></p>
                </div>
                <div className="fixed-phone" title="connect call" onClick={this.connectToCall}>
                    <PhoneIcon className="phone-icon" />
                </div>
                <div className="fixed-whatsapp" onClick={this.connectToWhatsApp} title="connect to what's app">
                    <WhatsAppIcon className="whats-app-icon" />
                </div>

                <div className="fixed-phone-mobile" title="connect call" onClick={this.connectToCall}>
                    <PhoneIcon className="phone-icon" />
                </div>
                <div className="fixed-whatsapp-mobile" onClick={this.connectToWhatsApp} title="connect to what's app">
                    <WhatsAppIcon className="whats-app-icon" />
                </div>

                <div className="fixed-slogan">
                    <p className="slogan-content">"Trust The Magic of New Beginnings"</p>
                </div>
                <div className="dashboard-header">
                    <img src="https://storage.googleapis.com/hirescripts/logoPng.png" className="logo-image" alt="logo" />
                    <p className="header-name">{commonTextFile.dashboardContent.headerData}</p>
                    {/* <p className="signin-option" title="log in">Log in</p> */}
                    {/* <Button appearance="ghost" className="signin-option" title="log in" onClick={this.featureAlert}>Log In</Button> */}
                </div>
                <div className="dashboard-body-section">
                    <div className="text-section">
                        {/* <p className="body-text">Online</p> */}
                        <p className="body-text">Online Learning Platform</p>
                    </div>
                    <div className="button-section">
                        <p className="register-button-quotes">Register for Instant access</p>
                        <Button appearance="ghost" className="register-button" title="click to register now" onClick={this.registerPopUp}>Register now</Button>
                    </div>
                </div>
                {/* <div className="intro-section">
                    <div className="intro-content">
                        <p className="intro-header">Introduction</p>
                        <p className="intro-body">Here You Are, At a Cross Road, <span>PATH NOT TAKEN</span>, are you a Student? Perhaps you are new Graduate
                            Ready to land on a new dream Job, maybe you are looking to redirect your career, maybe you feel that your skills
                            are being under utilized in your current position and longing for the better opportunity, whatever the reason,
                        </p>
                        <p className="intro-navigate">We are here to Guide you.</p>
                    </div>
                    <div className="image-section-intro">
                        <img src={introImg} className="intro-img" alt="introduction" />
                    </div>
                </div> */}
                <div className="about-section">
                    <div className="about-content-section">
                        <p className="about-header">About Us</p>
                        <p className="about-content">
                            <SendIcon className="about-icon" />
                            We Are Passionate To Bring Change In The Educational System And Increase The Employability Of The Country.
                        </p>
                        <p className="about-content">
                            <SendIcon className="about-icon" />
                            We provide Training and conduct offline exams, only for students registered from our partner colleges.
                        </p>
                        <p className="about-content">
                            <SendIcon className="about-icon" />
                            In addition to that, we do conduct workshops, Hackathons etc. to create competitive spirit and curiosity to learn and explore different things.
                        </p>
                        <p className="about-content">
                            <SendIcon className="about-icon" />
                            Special training and Personal care will be taken for students lagging the progress.
                        </p>
                    </div>
                    <div className="about-image-section">
                        <img src="https://storage.googleapis.com/hirescripts/intro.png" className="image-about" alt="about us" />
                    </div>
                </div>
                <div className="present-scenario-section">
                    <div className="right-section" style={{ width: "100%" }}>
                        <p className="title-para">Our Aim</p>
                        <p className="title-content"><span>HIRE SCRIPTS IS A DIGITAL LEARNING PLATFORM</span> Which Focuses On Reducing Skill Gap In The Job Market And Ensuring Every Student Of The Country To Get The Dream Job They Aspire For</p>
                    </div>
                </div>
                <div className="present-scenario-section">
                    <div className="right-section">
                        <p className="title-para">Our Vision</p>
                        <p className="title-content">To Provide Best Quality Training To Our Students By Hiring Highly Experienced Trainers Who Were Pioneers In Their Respective Field And Help Students Or Professionals To Land On Their Dream Job.
                        </p>

                    </div>
                    <div className="right-section">
                        <p className="title-para">Our Mission</p>
                        <p className="title-content">Success Through The Development Of Effective Programming Skills, Creativity, Critical Thinking In
                        A Accessible And Affordable Learning Environment.
                        </p>
                    </div>
                </div>
                <div className="expect-section">
                    <p className="expect-header">What To Expect From Us And Why We ?</p>
                    <p className="expect-content">
                        <SendIcon className="about-icon" />
                        Some Institutes Say Fake 100 % Placements, Some Say You Can Get Job By Preparing Instantly, But In Life Nothing Comes Instantly.</p>
                    <p className="expect-content">
                        <SendIcon className="about-icon" />
                        We Train Our Students To Acquire Skills And Knowledge , Not To Crack Previous Papers.</p>
                    <p className="expect-content">
                        <SendIcon className="about-icon" />
                        We Train Students To Be Industry Ready, Not Industry Prepared, One Cannot Prepare Students For An Industry Which Is Changing So Fast.</p>
                    <p className="expect-content">
                        <SendIcon className="about-icon" />
                        For Top Best Performers, We Will Guide Them To Make Innovative Projects With Us, For Absolutely Free.</p>
                </div>
                <div className="course-navigate-button">
                    <Button appearance="ghost" className="view-course-button" title="click to view all courses">Courses</Button>
                </div>
                <div className="list-of-status">
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Master of Data Science">
                                    {/* <MultilineChartIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/aptitude.jpg" className="status-header" style={{ width: "130px", height: "80px" }} alt="Aptitude" />
                                    <p className="status-header">Aptitude</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Quantitative</p>
                                <p className="status-body">Logical</p>
                                <p className="status-body">Verbal</p>
                                <p className="status-body">Soft skills</p>
                            </div>
                        </div>
                    </div>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Business analytics">
                                    {/* <LanguageIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/generalProgram.png" className="status-header" style={{ width: "100px", height: "80px" }} alt="generalProgram" />
                                    <p className="status-header">General Programming</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">C, C++</p>
                                <p className="status-body">HTML, CSS</p>
                                <p className="status-body">SQL</p>
                            </div>
                        </div>
                    </div>

                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Master of Artificial Intelligence">
                                    {/* <PersonAddIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/Ai-1.png" className="status-header" style={{ width: "120px", height: "80px" }} alt="AI" />
                                    <p className="status-header">Master of Artificial Intelligence</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Basics of Python</p>
                                <p className="status-body">Machine Learning/Data Science</p>
                                <p className="status-body">Deep Learning</p>
                            </div>
                        </div>
                    </div>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Master of Data Science">
                                    {/* <MultilineChartIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/datascience.png" className="status-header" style={{ width: "150px", height: "80px" }} alt="DataScience" />
                                    <p className="status-header">Master of Data Science</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Basics of Python</p>
                                <p className="status-body">Data science with python</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="list-of-status">
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Fullstack Java Script Developer">
                                    {/* <MenuBookIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/fullstack.png" className="status-header" style={{ width: "100px", height: "80px" }} alt="fullstack" />
                                    <p className="status-header">Fullstack Java Script Developer</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Basics of Java Script</p>
                                <p className="status-body">Type script, Advanced Java Script</p>
                                <p className="status-body">MEAN (Mongo,Express,Node,Angular)</p>
                            </div>
                        </div>
                    </div>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Python Developer(Backend)">
                                    {/* <AdbIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/python.jpg" className="status-header" style={{ width: "80px", height: "80px" }} alt="Python" />
                                    <p className="status-header">Python Developer(Backend)</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Basics of Python</p>
                                <p className="status-body">Flask</p>
                                <p className="status-body">Django</p>
                            </div>
                        </div>
                    </div>
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Java Developer">
                                    {/* <PieChartIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/java.png" className="status-header" style={{ width: "80px", height: "80px" }} alt="java" />
                                    <p className="status-header">Java Developer</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Basics of Java</p>
                                <p className="status-body">Advanced Java/Spring</p>
                                <p className="status-body">(Servlets/JSP/JDBC/JPA(Hibernate)</p>
                                <p className="status-body">/Spring-MVC/SpringBoot)</p>
                            </div>
                        </div>
                    </div>


                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Test Automation">
                                    {/* <BugReportIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/automation.jpg" className="status-header" style={{ width: "150px", height: "80px" }} alt="Automation" />
                                    <p className="status-header">Test Automation</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Basics of Manual Testing</p>
                                <p className="status-body">Basics of Test Automation</p>
                                <p className="status-body">Basics of Python, Selenium</p>
                                <p className="status-body">Robot Framework (UAT)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="list-of-status">
                    <div className="flip-card" style={{ width: "24%" }}>
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Data Analytics">
                                    {/* <BarChartIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/data-analytics.png" className="status-header" style={{ width: "150px", height: "80px" }} alt="dataAnalytics" />
                                    <p className="status-header">Data Analytics</p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Advanced Excel</p>
                                <p className="status-body">Tableau</p>
                            </div>
                        </div>
                    </div>
                    <div className="flip-card" style={{ width: "24%" }}>
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <div className="content-section" title="Master of Data Science">
                                    {/* <MultilineChartIcon className="icon-content" /> */}
                                    <img src="https://storage.googleapis.com/hirescripts/business.jpg" className="status-header" style={{ width: "100px", height: "80px" }} alt="Aptitude" />
                                    <p className="status-header">Business analytics </p>
                                </div>
                            </div>
                            <div className="flip-card-back">
                                <p className="status-body">Advanced Excel</p>
                                <p className="status-body">Business analytics</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="about-section" style={{margin:"5px 0px 10px 0px"}}>
                    <div className="about-content-section" style={{width:"100%", padding:"5px"}}>
                        <p className="about-header">Every Course includes</p>
                        {this.state.everyCourse.map((data, i) => {
                            return (
                                <p className="about-content">
                                    <SendIcon className="about-icon" />
                                    {data}
                                </p>
                            )
                        })}
                    </div>
                </div>
                <div className="footer-section">
                    <p className="footer-header">Contact Us</p>
                    <div className="footer-content">
                        <div className="left-sec">
                            <p className="footer-body">< PhoneIcon className="footer-icon" /> 08025043281</p>
                            <p className="footer-body">< WhatsAppIcon className="footer-icon" /> 9741191117</p>
                            <p className="footer-body">< EmailIcon className="footer-icon" /> contact@hirescripts.com</p>
                            <div className="address-section">
                                <div className="left-address-head">
                                    <p className="footer-body">Corporate Address:</p>
                                </div>
                                <div className="right-address-content">
                                    <p className="footer-body">#1108, No.39, 2nd floor, NGEF Lane, <br />Indira Nagar, First stage<br /> Bangalore -560038</p>
                                </div>
                            </div>
                            {/* <p className="footer-body">< LanguageIcon className="footer-icon" /> www.hirescripts.com</p> */}
                        </div>
                        <div className="right-sec">
                            <div style={{ textAlign: 'right', padding: "0px 30px 0px 0px", float: "right", cursor: "pointer" }}>
                                <p className="footer-body" onClick={this.faceBookRoute} title="click ">< FacebookIcon className="footer-icon" /> /Hirescripts</p>
                                <p className="footer-body" onClick={this.instPageRoute}>< InstagramIcon className="footer-icon" /> /Hirescripts</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>

                </div>
            </div>
        )
    }
}
export default Dashboard;